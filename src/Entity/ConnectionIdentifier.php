<?php

namespace App\Entity;

use App\Repository\ConnectionIdentifierRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;

#[ORM\Entity(repositoryClass: ConnectionIdentifierRepository::class)]
#[Table]
#[UniqueConstraint(name: 'con_id_assoc_unique', columns: ['user_id', 'identifier'])]
class ConnectionIdentifier
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'connectionIdentifiers')]
    #[ORM\JoinColumn(nullable: false)]
    private $user;
    #[ORM\Column(type: 'datetime')]
    private $lastUsed;
    #[ORM\Column(type: 'string', length: 32)]
    private $identifier;
    public function getId(): ?int
    {
        return $this->id;
    }
    public function getUser(): ?User
    {
        return $this->user;
    }
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
    public function getLastUsed(): ?\DateTimeInterface
    {
        return $this->lastUsed;
    }
    public function setLastUsed(\DateTimeInterface $lastUsed): self
    {
        $this->lastUsed = $lastUsed;

        return $this;
    }
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }
    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }
}
